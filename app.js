const yargs = require("yargs");
const commands = require("./commands");

let command = yargs.argv._[0];
let shape = yargs.argv.shape;
let shapeType = yargs.argv.shapeType;
let color = yargs.argv.color;
let base = yargs.argv.base;
let height = yargs.argv.height;
let side = yargs.argv.side;
let radious = yargs.argv.radious;
let base1 = yargs.argv.base1;
let base2 = yargs.argv.base2;

if (command === "add") {
  commands.add(shape, color, base, height, radious, side, base1, base2);
} else if (command === "list") {
  commands.list(shapeType);
} else if (command === "shapehelp") {
  commands.shapehelp();
} else {
  console.log(
    "make sure you are using the correct commands run $node app shapehelp"
  );
}
