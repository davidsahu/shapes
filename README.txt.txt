
Welcome to figure shapes :)

Example for create a triangle shape run the command: $node app add --shape=triangle --color=<optional> --base=22 --height=12
Example for create a square shape run the command: $node app add --shape=square --color=<optional> --side=33
Example for create a circle shape run the command: $node app add --shape=circle --color=<optional> --radious=44
Example for create a rectangle shape run the command: $node app add --shape=rectangle --color=<optional> --base=22 --height=12
Example for create a trapezoid shape run the command: $node app add --shape=trapezoid --color=<optional> --base1=32 --base2=44 --height=12
For list all the shapes sort by area run the command: $node app list
For list a specific shape sort by area for example rectangle run the command: $node app list --shapeType=rectangle

Happy coding (: