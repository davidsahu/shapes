const fs = require("fs");

const readFile = fileName => {
  try {
    return JSON.parse(fs.readFileSync("shapeList.json"));
  } catch (error) {
    return [];
  }
};

const colors = [
  "red",
  "yellow",
  "blue",
  "red",
  "pink",
  "purple",
  "green",
  "brown"
];

const add = (shape, color, base, height, radious, side, base1, base2) => {
  console.log(shape);
  console.log(color);
  colors.map(val => {
    if (color === val) {
      if (shape === "triangle") {
        let shapeList = readFile("shapeList.json");
        let area = (base * height) / 2;
        shapeList.push({ shape, color, base, height, area });
        fs.writeFileSync("shapeList.json", JSON.stringify(shapeList));
        console.log(shapeList);
      } else if (shape === "square") {
        let shapeList = readFile("shapeList.json");
        let area = Math.pow(side, 2);
        shapeList.push({ shape, color, side, area });
        fs.writeFileSync("shapeList.json", JSON.stringify(shapeList));
        console.log(shapeList);
      } else if (shape === "circle") {
        let shapeList = readFile("shapeList.json");
        let area = Math.PI * Math.pow(radious, 2);
        shapeList.push({ shape, color, radious, area });
        fs.writeFileSync("shapeList.json", JSON.stringify(shapeList));
        console.log(shapeList);
      } else if (shape === "rectangle") {
        let shapeList = readFile("shapeList.json");
        let area = base * height;
        shapeList.push({ shape, color, base, height, area });
        fs.writeFileSync("shapeList.json", JSON.stringify(shapeList));
        console.log(shapeList);
      } else if (shape === "trapezoid") {
        let shapeList = readFile("shapeList.json");
        let area = (base1 * height) / 2 + (base2 * height) / 2;
        shapeList.push({ shape, color, base1, base2, height, area });
        fs.writeFileSync("shapeList.json", JSON.stringify(shapeList));
        console.log(shapeList);
      } else {
        console.log("run the command $node app shapehelp");
      }
    }
  });
};

const list = shapeType => {
  let shapeList = readFile("shapeList.json");

  console.log(shapeType);
  if (shapeType === undefined) {
    function comparar(a, b) {
      return a.area - b.area;
    }
    console.log(shapeList.sort(comparar));
    //   console.log(shapeList);
  } else {
    let index = shapeList.findIndex(x => x.shape === shapeType);
    let search = shapeList.filter(({ shape }) => shape === shapeType);

    // console.log(search);
    if (index !== -1) {
      //console.log(search);
      function comparar(a, b) {
        return a.area - b.area;
      }
      console.log(search.sort(comparar));
    } else {
      console.log(`shape ${shapeType} doesnt found`);
    }
  }
};

const shapehelp = () => {
  console.log("Welcome to figure shapes :)");
  console.log(
    "Example for create a triangle shape run the command: $node app add --shape=triangle --color=<optional> --base=22 --height=12"
  );
  console.log(
    "Example for create a square shape run the command: $node app add --shape=square --color=<optional> --side=33"
  );
  console.log(
    "Example for create a circle shape run the command: $node app add --shape=circle --color=<optional> --radious=44"
  );
  console.log(
    "Example for create a rectangle shape run the command: $node app add --shape=rectangle --color=<optional> --base=22 --height=12"
  );
  console.log(
    "Example for create a trapezoid shape run the command: $node app add --shape=trapezoid --color=<optional> --base1=32 --base2=44 --height=12"
  );
  console.log(
    "For list all the shapes sort by area run the command: $node app list"
  );
  console.log(
    "For list a specific shape sort by area for example rectangle run the command: $node app list --shapeType=rectangle"
  );
  console.log("Happy coding (:");
};

module.exports = {
  add,
  list,
  shapehelp
};
